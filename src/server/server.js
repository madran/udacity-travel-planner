const port = 9000;

const express = require('express');

const server = express();
server.use(express.json());
server.use(express.urlencoded({ extended: false }));

server.get('/ok', (req, res) => {

    console.log('ok');
    res.send("OK");
})

server.listen(
    port,
    () => {
        console.log('Server starting on port: ' + port);
    }
);