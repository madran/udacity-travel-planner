import html from '../../views/components/destination-input.html';
import css from '../../sass/components/destination-input.scss';

class DestinationInput extends HTMLElement {
    constructor() {
        super();

        const destinationInputHTML = document.createElement('template');
        destinationInputHTML.innerHTML = `<style>${css}</style>` + html;

        this.attachShadow({mode: 'open'})
        this.shadowRoot.appendChild(destinationInputHTML.content.cloneNode(true));
        this.shadowRoot
            .querySelector('input[name=location]')
            .setAttribute('name', 'location-' + this.getAttribute('name'));
        this.shadowRoot
            .querySelector('input[name=date]')
            .setAttribute('name', 'date-' + this.getAttribute('name'));
    }
}

window.customElements.define('destination-input', DestinationInput);