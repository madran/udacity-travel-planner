const path = require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    mode: 'development',
    entry: './src/client/client.js',
    target: 'web',
    devServer: {
        port: 3000,
        proxy : {

        }
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/client/views/layout.html'
        })
    ],
    module: {
        rules: [
            {
                test: /.(sc|c)ss$/,
                exclude: path.resolve(__dirname, 'src/client/sass/components'),
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /.(sc|c)ss$/,
                include: path.resolve(__dirname, 'src/client/sass/components'),
                use: ['to-string-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /.html$/,
                use: ['html-loader']
            }
        ]
    }
}