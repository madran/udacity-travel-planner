const nodeExternals = require("webpack-node-externals");

const NodemonPlugin = require("nodemon-webpack-plugin");

module.exports = {
    mode: "development",
    target: "node",
    entry: "./src/server/server.js",
    devServer: {
        port: 9000
    },
    externals: [nodeExternals()],
    plugins: [
        new NodemonPlugin()
    ]
}